import { CircularProgress, Divider, Stack, Typography } from "@mui/material"
import { ProductCard } from "../components/card/ProductCard"
import { LandingMoreClassList } from "../components/contains/LandingMoreClassList"
import { blackColor } from "../utils/color"
import { FooterPage } from "../components/footer/FooterPage"
import { useParams } from "react-router"
import { useDetailCourse } from "../hooks/useDetailCourse"

export const DetailClass = () => {
    const { id } = useParams();
    const { detailCourse, otherCourseData } = useDetailCourse(id);

    if (!detailCourse) {
        return (
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh' }}>
                <CircularProgress />
            </div>
        );
    }


    return (
        <Stack minHeight={'100vh'} width={'100%'} marginTop={'20px'}>
            <ProductCard Product={detailCourse} />
            <Stack padding={'0px 5%'} margin={'70px 0px'}>
                <Typography variant="h5" color={blackColor} fontWeight={'600'} marginBottom={'15px'}>
                    Deskripsi
                </Typography>
                <Typography variant="body1" color={blackColor} fontWeight={'400'} textAlign={'justify'}>
                    {detailCourse.description}
                </Typography>
            </Stack>

            <Divider />
            <LandingMoreClassList title="Kelas lain yang mungkin kamu suka" otherList={otherCourseData} />
            <FooterPage />
        </Stack>

    )
}