import { ListClassMenuThumbnail } from "../components/contains/ListClassMenuThumbnail"
import { TitlewDesc } from "../components/contains/TitlewDesc"
import { ListClass } from "../components/contains/ListClass"
import { FooterPage } from "../components/footer/FooterPage"
import { Box, CircularProgress, Divider, Stack, Typography } from "@mui/material"
import { useParams } from 'react-router-dom';
import { imgUrl } from "../utils/api"
import { useDetailCategory } from "../hooks/useDetailCategory"
import { blackColor } from "../utils/color"

export const ClassListPage = () => {
    const { id } = useParams();
    const { categoryData, isCategoryLoading, } = useDetailCategory(id)

    return (
        <div>
            {categoryData === null ? (<div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh' }}><CircularProgress /></div>
            ) : (
                <Box minHeight={'100vh'} width={'100%'} justifyContent={'space-between'}>
                    <ListClassMenuThumbnail imageURL={`${imgUrl}${categoryData.image}`} categoryType={categoryData.name} />
                    <Stack padding={'0px 5%'} margin={'70px 0px'}>
                        <Typography variant="h5" color={blackColor} fontWeight={'600'} marginBottom={'15px'}>
                            {`${categoryData.name} Class`}
                        </Typography>
                        <Typography variant="body1" color={blackColor} fontWeight={'400'} textAlign={'justify'}>
                            {categoryData.description}
                        </Typography>
                    </Stack>
                    <Divider />
                    <ListClass categoryId={categoryData.id} title={categoryData.name} />
                    <FooterPage />
                </Box>)}
        </div>
    )
}
