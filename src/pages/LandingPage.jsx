import { useEffect, useState } from "react"
import { LandingClassCategory } from "../components/contains/LandingClassCategory"
import { LandingClassExplore } from "../components/contains/LandingClassExplore"
import { LandingImageAdditional } from "../components/contains/LandingImageAdditional"
import { LandingImageThumbnail } from "../components/contains/LandingImageThumbnail"
import { FooterPage } from "../components/footer/FooterPage"
import { Box, CircularProgress, Container } from "@mui/material"
import { useListLanding } from "../hooks/useListLanding"

export const LandingPage = () => {
    const { ListCourseData, allCategoryData} = useListLanding();

    if (!ListCourseData || !allCategoryData) {
        return (
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh' }}>
                <CircularProgress />
            </div>
        );
    }

    return (

        <Box>
            <LandingImageThumbnail />
            <LandingClassExplore title="Explore kelas favorit" list={ListCourseData} />

            <LandingClassCategory data={allCategoryData} />
            <LandingImageAdditional />
            <FooterPage />
        </Box>
    )
}
