import { useState } from "react";
import { Box, Container, Stack, TextField, Typography, IconButton, InputAdornment, } from "@mui/material"
import { gray1Color } from "../utils/color"
import FlatButton from "../components/button/FlatButton"
import BorderButtonForm from "../components/button/BorderButton"
import { useNavigate } from "react-router-dom"
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { useAuth } from "../hooks/useAuth";

export const NewPassPage = () => {
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const navigate = useNavigate()

  const { CreateNewPassword, isLoading } = useAuth();

  const submitNewPass = async (e) => {
    e.preventDefault();
    const currentURL = window.location.href;
    const url = new URL(currentURL);
    const email = url.searchParams.get('email');
    const token = url.searchParams.get('token');
    console.log('Email:', email);
    console.log('Token:', token);
    console.log('Password:', newPassword);

    if (!isLoading) {
      if (newPassword != confirmPassword) {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Password tidak sama",
        });
      } else {
        CreateNewPassword(email, newPassword, token)
      }
    }
  };

  const handleCancel = () => {
    navigate("/login")
  }

  const validatePassword = () => {
    if (!newPassword) {
      setPasswordError("Password fields cannot be blank");
      return false;
    }

    if (!passContain()) {
      return false;
    }
    if (!confirmPassword) {
      setPasswordError("Please confirm you password");
      return false;
    }

    if (newPassword !== confirmPassword) {
      setPasswordError("Passwords do not match");
      return false;
    }

    setPasswordError("");
    return true;
  };

  const passContain = () => {
    if (newPassword.length == 0) {
      setPasswordError("Password fields cannot be blank");
      return false;
    }

    if (newPassword.length < 8) {
      setPasswordError("Password must be at least 8 characters long");
      return false;
    }

    if (newPassword.includes(" ")) {
      setPasswordError("Password cannot contain spaces");
      return false;
    }

    const passwordRegex = /^(?=.*[A-Z])(?=.*[!@#$%^&*])(?=.*[0-9]).{8,}$/;
    if (!passwordRegex.test(newPassword)) {
      setPasswordError(
        "Password must contain at least 1 uppercase, 1 special character, 1 number."
      );
      return false;
    }
    setPasswordError("");
    return true
  };

  const handleTogglePasswordVisibility = (field) => {
    if (field === "newPassword") {
      setShowNewPassword((prevShowPassword) => !prevShowPassword);
    } else if (field === "confirmPassword") {
      setShowConfirmPassword((prevShowPassword) => !prevShowPassword);
    }
  };


  const handlePasswordBlur = () => {
    passContain();
  };

  return (
    <Container maxWidth="sm">
      <Stack sx={{
        display: "flex",
        flexDirection: "column",
        marginTop: '182px',
        gap: "40px",
      }}
      >
        <Box sx={{
          display: "flex",
          justifyContent: "start",
          flexDirection: "column",
          gap: "60px",
        }}
        >
          <Typography color={gray1Color} variant="h5">
            Buat Password
          </Typography>
          <form onSubmit={submitNewPass} style={{
            display: "flex",
            flexDirection: "column",
            gap: "24px",
          }}
          >
            <TextField
              fullWidth
              label="Password Baru"
              variant="outlined"
              margin="none"
              size="small"
              type={showNewPassword ? "text" : "password"}
              value={newPassword}
              onChange={(e) => setNewPassword(e.target.value)}
              onBlur={() => handlePasswordBlur("newPassword")}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      onClick={() => handleTogglePasswordVisibility("newPassword")}
                      edge="end"
                    >
                      {showNewPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
            <TextField
              fullWidth
              label="Konfirmasi Password Baru"
              variant="outlined"
              margin="none"
              size="small"
              type={showConfirmPassword ? "text" : "password"}
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
              onBlur={() => validatePassword()}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      onClick={() => handleTogglePasswordVisibility("confirmPassword")}
                      edge="end"
                    >
                      {showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </form>
        </Box>
        {passwordError && (
          <Box sx={{ marginBottom: 2 }}>
            <Typography color="error" variant="body2">
              {passwordError}
            </Typography>
          </Box>
        )}
        <Box sx={{
          display: "flex",
          justifyContent: "flex-start",
          width: '300px',
          gap: '24px'
        }}
        >
          <BorderButtonForm action={handleCancel} title={"Batal"} />
          <FlatButton action={submitNewPass} title={isLoading ? "Loading" : "Submit"} />
        </Box>
      </Stack>
    </Container>
  )
}
