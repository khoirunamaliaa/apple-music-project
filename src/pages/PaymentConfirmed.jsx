import { Box, Container, Stack, Typography, CardMedia, Button} from "@mui/material"
import { irisColor, gray2Color } from "../utils/color"
import EmailLogo from '../assets/EmailPagePic.png'
import { useNavigate } from "react-router-dom"
import { ArrowRightAltRounded, Home, HomeMini, KeyboardArrowRightRounded } from "@mui/icons-material"

export const PaymentConfirmed = () => {
    const navigate = useNavigate()

    const handleConfirmed = () => {
        navigate("/invoice")
    }
    const handleBack = () => {
      navigate("/")
  }

    return (
        <Container maxWidth={"sm"}>
            <Stack sx={{
              display: "flex",
              justifyContent: "start",
              alignItems:'center',
              flexDirection: "column",
              marginTop:'122px',
              gap: "40px",}}
            >
                <CardMedia
                component="img"
                alt="Image" 
                height="250"
                image={EmailLogo}
                sx={{objectFit: "Contain"}}
                />

                <Box sx={{
                  display: "flex",
                  justifyContent: "start",
                  alignItems:'center',
                  flexDirection: "column",
                  gap: "40px",}}
                >
                    <Box sx={{
                      display: "flex",
                      justifyContent: "start",
                      alignItems:'center',
                      flexDirection: "column",
                      gap: "8px",
                      width:'100%',
                    }}
                    >
                        <Typography color={irisColor} variant="h5">
                          Pembelian Berhasil
                        </Typography>
                        <Typography color={gray2Color} variant="subtitle1">
                          Yey! Kamu telah berhasil membeli kursus di Apel Music
                        </Typography>
                    </Box>
                    <Box height={'55px'} width={'100%'} sx={{display: "flex", justifyContent: "center", flexDirection: "row", alignItems:'center', gap: "24px",}}>
                      <Button variant="outlined" style={{height:'100%', width:'161px', textTransform:'none', fontSize:'15px', fontWeight:'600', gap: "8px",}}  onClick={handleBack}>
                        <Home fontSize="small"/>
                        Ke Beranda
                      </Button>
                      <Button variant="contained" style={{height:'100%', width:'161px',textTransform:'none', fontSize:'15px', fontWeight:'600',}} onClick={handleConfirmed}>
                        <KeyboardArrowRightRounded/>
                        Buka Invoice
                      </Button>
                    </Box>
                </Box>
            </Stack>
        </Container>
    )
}
