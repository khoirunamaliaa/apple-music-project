import { CircularProgress, Grid, Stack, Typography } from "@mui/material"
import { Link, useParams } from "react-router-dom"
import { blackColor, gray2Color, gray3Color, irisColor } from "../utils/color"
import { DetailInvoiceTable } from "../components/table/DetailInvoiceTable"
import { FooterPage } from "../components/footer/FooterPage"
import { useEffect, useState } from "react"
import { useInvoice } from "../hooks/useListInvoice";
import moment from 'moment';
import 'moment/locale/id';


export const DetailInvoice = () => {
    const { no } = useParams();
    const { getDetailInvoice, isListLoading, detailInvoice, } = useInvoice();
    const [totalPrice,setTotalPrice]=useState(0);


    useEffect(() => {
        if (detailInvoice && detailInvoice.listInvoice) {
            const TotalPrice = detailInvoice.listInvoice.reduce((accumulator, data) => accumulator + data.price, 0);
            setTotalPrice(TotalPrice);
        } else {
            getDetailInvoice(no);
            setTotalPrice(0);
        }
    }, [no,detailInvoice.listInvoice])
    if (isListLoading || !detailInvoice) {
        return (
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh' }}>
                <CircularProgress />
            </div>
        );
    }

    return (
        <Stack minHeight={'100vh'} width={'100%'} justifyContent={'space-between'}>
            <Stack marginTop={'68px'} flex={1} justifyContent={'start'} alignItems={'start'} padding={'10px 5%'}>
                <Stack flexDirection={'row'} marginTop={'30px'}>
                    <Link to={'/'} style={{ textDecoration: 'none' }}>
                        <Typography variant="body1" fontWeight={'600'} color={gray3Color} marginRight={'5px'}>
                            {"Home >"}
                        </Typography>
                    </Link>
                    <Link to={'/invoice'} style={{ textDecoration: 'none' }}>
                        <Typography variant="body1" fontWeight={'600'} color={gray3Color} marginRight={'5px'}>
                            {"Invoice >"}
                        </Typography>
                    </Link>
                    <Typography variant="body1" fontWeight={'600'} color={irisColor} >
                        Rincian Invoice
                    </Typography>
                </Stack>
                <Grid container marginY={'20px'}>
                    <Grid item xs={12} md={6}>
                        <Typography variant="body1" fontWeight={'700'} color={gray2Color} >
                            Rincian Invoice
                        </Typography>
                        <Typography variant="body1" fontWeight={'500'} color={gray2Color} >
                            No. Invoice : {detailInvoice.noInvoice}
                        </Typography>
                        <Typography variant="body1" fontWeight={'500'} color={gray2Color} >
                            Date : {detailInvoice.createAt ? moment(detailInvoice.createAt).locale('id').format('dddd, DD MMMM YYYY [at] HH:mm') : 'Loading...'}
                        </Typography>
                    </Grid>
                    <Grid item xs={12} md={6} justifyContent={{sm:'start', md: 'end'}} display={'flex'} >
                        <Typography variant="body1" sx={{ fontWeight:{xs:'500', md: '700' },}} color={gray2Color}>
                            Total Price : IDR {totalPrice ? totalPrice.toLocaleString() : 'Loading...'}
                        </Typography>
                    </Grid>
                </Grid>
                <DetailInvoiceTable loading={isListLoading} list={detailInvoice.listInvoice} />
            </Stack>
            <FooterPage />
        </Stack>
    )
}