import { Box, CircularProgress, Stack, Typography } from "@mui/material"
import { blackColor, gray3Color, irisColor } from "../utils/color"
import { FooterPage } from "../components/footer/FooterPage"
import { useListMyClass } from "../hooks/useListMyClass"
import { imgUrl } from "../utils/api"
import moment from 'moment';
import 'moment/locale/id';

export const MyClass = () => {
    const { isListCourseLoading, ListCourseData } = useListMyClass();

    if (isListCourseLoading) {
        return (
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh' }}>
                <CircularProgress />
            </div>
        );
    }

    return (
        <Box sx={{ display:'flex', flexDirection:'column', minHeight:'100vh' }}>
            <Stack width={'100%'} justifyContent={'space-between'} style={{ flex:1 }}>
                <Box sx={{ marginTop: '98px', }}>
                    {ListCourseData && ListCourseData.length > 0 ? (
                        ListCourseData.map((data, index) => (
                            <Stack padding={'20px 5%'} flexDirection={'row'} alignItems={'center'} sx={{ marginBottom: '0px !important', borderBottom: '1px solid #E0E0E0' }}>
                                <Box sx={{
                                    width: { lg: '180px', md: '180px', sm: '100px', xs: '100px' },
                                    aspectRatio: '16/10',
                                    height: '100px',
                                    backgroundImage: `url(${imgUrl}${data.image})`,
                                    backgroundPosition: 'center',
                                    backgroundSize: 'cover',
                                    backgroundRepeat: 'no-repeat',
                                    borderRadius: '16px'
                                }} />
                                <Stack marginLeft={'10px'}>
                                    <Typography style={{
                                        fontWeight: '400',
                                        fontSize: '16px',
                                        color: gray3Color(),
                                        typography: { lg: "body1", md: "body1", sm: "body2", xs: "body2" },
                                    }}>
                                        {data.category}
                                    </Typography>
                                    <Typography style={{
                                        fontWeight: '600',
                                        color: blackColor(),
                                        typography: { lg: "h5", md: "h5", sm: "body1", xs: "body1" }
                                    }}>
                                        {data.title}
                                    </Typography>
                                    <Typography style={{
                                        fontWeight: "400",
                                        color: irisColor(),
                                        typography: { lg: "h5", md: "h5", sm: "body1", xs: "body1" },
                                    }}>
                                        Jadwal : {moment(data.schedule).locale('id').format('dddd, DD MMMM YYYY [at] HH:mm')}
                                    </Typography>

                                </Stack>
                            </Stack>
                        ))
                    ) : (
                        <Typography style={{ marginTop: '50px', fontWeight: '600', color: blackColor(), textAlign: 'center', typography: { lg: "h5", md: "h5", sm: "body1", xs: "body1" } }}>
                            No Item Available.
                        </Typography>
                    )}
                </Box>
            </Stack>
            <FooterPage />
        </Box>


    )
}