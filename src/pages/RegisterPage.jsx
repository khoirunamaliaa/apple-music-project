import { Box, Container, IconButton, InputAdornment, Stack, TextField, Typography } from "@mui/material"
import { blackColor, blueColor, gray1Color, gray2Color } from "../utils/color"
import FlatButton from "../components/button/FlatButton"
import { Link } from "react-router-dom"
import { useState } from "react"
import { useAuth } from "../hooks/useAuth"
import { Visibility, VisibilityOff } from "@mui/icons-material"
import Swal from "sweetalert2"

export const RegisterPage = () => {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [passwordError, setPasswordError] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const { register, isLoading } = useAuth();


  // const navigate = useNavigate()

  const isValidateEmail = (email) => {
    // Regular expression for basic email validation
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (!emailRegex.test(email)) {
      return false; // Basic email format validation failed
    }

    // Extract domain from the email address
    const [, domain] = email.split('@');

    // Check if the domain is allowed (add more domains as needed)
    const allowedDomains = ['yahoo.com', 'gmail.com', 'hotmail.com', 'outlook.com', 'aol.com'];
    if (!allowedDomains.includes(domain)) {
      return false; // Domain validation failed
    }

    return true;
  };

  const submitRegister = async (e) => {
    const trimmedemail = email.trim()
    const trimmedname = name.trim()
    e.preventDefault()
    console.log(email + password + name + confirmPassword)
    // console.log(isValidateEmail)
    if (!isLoading) {
      if (password != confirmPassword) {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Password tidak sama",
        });
      }
      else if (trimmedname== "") {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Nama Tidak Valid",
        });
      }
      else if (isValidateEmail(trimmedemail)== "") {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Email Tidak Valid",
        });
        return
      }
      else {
        // console.log("test2")
        register(name, trimmedemail, password)
      }

    }

  }


  const validatePassword = () => {
    if (!password) {
      setPasswordError("Password fields cannot be blank");
      return false;
    }

    if (!passContain()) {
      return false;
    }
    if (!confirmPassword) {
      setPasswordError("Please confirm you password");
      return false;
    }

    // if (password !== confirmPassword) {
    //   setPasswordError("Passwords do not match");
    //   return false;
    // }

    setPasswordError("");
    return true;
  };

  const passContain = () => {
    if (password.length == 0) {
      setPasswordError("Password fields cannot be blank");
      return false;
    }

    if (password.length < 8) {
      setPasswordError("Password must be at least 8 characters long");
      return false;
    }

    if (password.includes(" ")) {
      setPasswordError("Password cannot contain spaces");
      return false;
    }

    const passwordRegex = /^(?=.*[A-Z])(?=.*[!@#$%^&*])(?=.*[0-9]).{8,}$/;
    if (!passwordRegex.test(password)) {
      setPasswordError(
        "Password must contain at least 1 uppercase, 1 special character, 1 number."
      );
      return false;
    }
    setPasswordError("");
    return true
  };

  const handleTogglePasswordVisibility = (field) => {
    if (field === "newPassword") {
      setShowPassword((prevShowPassword) => !prevShowPassword);
    } else if (field === "confirmPassword") {
      setShowConfirmPassword((prevShowPassword) => !prevShowPassword);
    }
  };


  const handlePasswordBlur = () => {
    passContain();
  };

  return (
    <Container maxWidth="sm">
      <Stack
        sx={{
          display: "flex",
          flexDirection: "column",
          marginTop: '182px',
          marginBottom: '100px',
          gap: "40px",
        }}
      >
        <Box sx={{
          display: "flex",
          justifyContent: "start",
          flexDirection: "column",
          gap: "60px",
        }}>
          <Box sx={{
            display: "flex",
            justifyContent: "start",
            flexDirection: "column",
            gap: "16px",
          }}>
            <Typography color={gray1Color} variant="h5">
              Selamat Datang Musikers!
            </Typography>
            <Typography variant="subtitle1" color={gray2Color}>
              Yuk daftar terlebih dahulu akun kamu
            </Typography>
          </Box>
          <form onSubmit={submitRegister} style={{
            display: "flex",
            flexDirection: "column",
            gap: "24px",
          }}>
            <TextField
              fullWidth
              required
              label="Masukkan Nama Lengkap"
              variant="outlined"
              margin="none"
              size="small"
              onChange={e => setName(e.target.value)}
              value={name}
            />
            <TextField
              fullWidth
              required
              label="Masukkan Email"
              variant="outlined"
              margin="none"
              size="small"
              onChange={e => setEmail(e.target.value)}
              value={email}
            />
            <TextField
              fullWidth
              label="Masukan Password"
              variant="outlined"
              margin="none"
              size="small"
              type={showPassword ? "text" : "password"}
              onChange={e => setPassword(e.target.value)}
              onBlur={() => handlePasswordBlur("Password")}
              value={password}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      onClick={() => handleTogglePasswordVisibility("newPassword")}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
            <TextField
              fullWidth
              label="Konfirmasi Password"
              variant="outlined"
              margin="none"
              size="small"
              type={showConfirmPassword ? "text" : "password"}
              onChange={e => setConfirmPassword(e.target.value)}
              onBlur={() => validatePassword()}
              value={confirmPassword}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      onClick={() => handleTogglePasswordVisibility("confirmPassword")}
                      edge="end"
                    >
                      {showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
            {passwordError && (
              <Box sx={{ marginBottom: 2 }}>
                <Typography color="error" variant="body2">
                  {passwordError}
                </Typography>
              </Box>
            )}
            <Box sx={{
              display: "flex",
              flexDirection: {xs:'column', md:'row'},
              justifyContent: "flex-start",
              width: '100%',
              gap: {xs:'10px', md:'40px'},
              alignItems: {xs:'flex-start', md:'center'},

            }}
            >
              <FlatButton title={isLoading ? "Loading" : "Daftar"} />
              <Typography variant="caption" display="flex" gutterBottom  sx={{
                color: blackColor(), textAlign: 'left'
              }}>
                Sudah punya akun?
                <Link style={{ textDecoration: 'none' }} to={'/login'}>
                  <Typography variant="caption" sx={{ color: blueColor() }}>
                    Login disini
                  </Typography>
                </Link>
              </Typography>
            </Box>
          </form>
        </Box>
      </Stack>
    </Container>
  )
}