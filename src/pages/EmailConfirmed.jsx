import { Box, Container, Stack, Typography, CardMedia} from "@mui/material"
import { irisColor, gray2Color } from "../utils/color"
import EmailLogo from '../assets/EmailPagePic.png'
import WebButton from "../components/button/WebButton"
import { useNavigate } from "react-router-dom"
import { useAuth } from "../hooks/useAuth"
import { useEffect } from "react"

export const EmailConfirmed = () => {
  const { ConfirmPassword, isLoading} = useAuth();

  useEffect(() => {
    const currentURL = window.location.href;
    const url = new URL(currentURL);
    const verifToken = url.searchParams.get('verificationToken');
    ConfirmPassword(verifToken)
  },[])

    const navigate = useNavigate()

    return (
        <Container maxWidth="sm">
            <Stack sx={{
              display: "flex",
              justifyContent: "start",
              alignItems:'center',
              flexDirection: "column",
              marginTop:'122px',
              gap: "40px",}}
            >
                <CardMedia
                component="img"
                alt="Image" 
                height="250"
                image={EmailLogo}
                sx={{objectFit: "Contain"}}
                />

                <Box sx={{
                  display: "flex",
                  justifyContent: "start",
                  alignItems:'center',
                  flexDirection: "column",
                  gap: "40px",}}
                >
                    <Box sx={{
                      display: "flex",
                      justifyContent: "start",
                      alignItems:'center',
                      flexDirection: "column",
                      gap: "8px",
                      width:'100%',
                    }}
                    >
                        <Typography color={irisColor} variant="h5">
                          Pendaftaran Akun Berhasil
                        </Typography>
                        <Typography color={gray2Color} variant="subtitle1">
                          Silahkan Login terlebih dahulu untuk masuk ke aplikasi
                        </Typography>
                    </Box>
                    <WebButton title={isLoading ? "Loading" : "Masuk Sekarang"} action={() => isLoading ? null : navigate('/login')} />
                </Box>
            </Stack>
        </Container>
    )
}
