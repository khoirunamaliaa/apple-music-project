import { useState } from 'react';
import { Checkbox, Box, Typography, Button, Container, Stack, CircularProgress } from '@mui/material';
import PaymentDialog from '../components/modal/PaymentDialog';
import { useCartFunction } from '../hooks/useCartFunction';
import { CartCard } from '../components/card/CartCard';
import { irisColor } from '../utils/color';
import Swal from 'sweetalert2';

export const CheckoutPage = () => {
  const {
    listCart,
    proceedPayment,
    handleSelectCart,
    handleSelectAllCart,
    handleDeleteCart,
    isAllChecked,
    totalPrice,
  } = useCartFunction();

  const [openPaymentDialog, setOpenPaymentDialog] = useState(false);

  const handlePay = () => {
    if (proceedPayment){
      setOpenPaymentDialog(true);
    } else {
      Swal.fire({
        icon: "warning",
        title: "No Cart Selected",
        text: "Please select at least one cart to proceed",
    });
    }
  };

  if (!listCart) {
    return (
      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh' }}>
        <CircularProgress />
      </div>
    );
  }

  return (
    <Box>
      <Container sx={{ marginTop: '132px', marginBottom: '104px' }}>
        <Stack sx={{
          display: "flex",
          flexDirection: "column",
          gap: "24px",
        }}
        >
          <Box display="flex" alignItems="center" mb={1} sx={{ marginBottom: '0px !important', borderBottom: '1px solid #E0E0E0' }}>
            <Checkbox checked={isAllChecked} onChange={handleSelectAllCart} />
            <Typography>Pilih Semua</Typography>
          </Box>

          <CartCard listCart={listCart} handleSelectCart={handleSelectCart} handleDeleteCart={handleDeleteCart} />

        </Stack>
      </Container>
      <Box sx={{
        position: 'fixed',
        bottom: 0,
        left: 0,
        maxHeight: '104px',
        width: '100%',
        backgroundColor: '#fff',
        borderTop: '1px solid #BDBDBD',
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        padding: { xs: "15px", md: "35px 30px" },
        alignItems: "center",
        boxShadow: '0px 0px 10px #BDBDBD',
      }}>

        <Box sx={{
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        gap: { xs: "10px", md: "24px" },
        }}>
          <Typography variant="h6" fontSize={{xs:"12px", sm:"18px"}} mt={0}>
            Total Biaya:
          </Typography>
          <Typography variant="h5" fontSize={{xs:"16px", sm:"24px"}} mt={0} color={irisColor()} fontWeight={500}>
            IDR {totalPrice.toLocaleString()}
          </Typography>
        </Box>

        <Button variant="contained" color="primary" onClick={handlePay} mt={2} sx={{maxheight: "44px", maxWidth:"233px", fontSize:{xs:"10px", sm:"16px"}, fontWeight:"500px", }}>
          Bayar Sekarang
        </Button>
      </Box>
      <PaymentDialog
        open={openPaymentDialog}
        onClose={setOpenPaymentDialog}
      />
    </Box>
  );
};
