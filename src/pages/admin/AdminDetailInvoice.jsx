import { 
    Paper,
    Table, 
    TableBody, 
    TableCell, 
    TableContainer, 
    TableHead, 
    TableRow } from "@mui/material"
import { useParams } from "react-router-dom"
import { useAdminDetail } from "../../hooks/useAdminDetail"
import moment from "moment"
import 'moment/locale/id';

export const AdminDetailInvoice = () => {
    const { no } = useParams()
    const { dataAdmin, isLoading } = useAdminDetail(no)
    
    if (isLoading) {
        return <h1>Loading ...</h1>
    }

    return (
        <div>
            <TableContainer component={Paper}>
                <Table sx={{ pb: 15, minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center">Course Name</TableCell>
                            <TableCell align="center">Category</TableCell>
                            <TableCell align="center">Schedule</TableCell>
                            <TableCell align="center">Price</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            dataAdmin.listInvoice.map((value, index) => {
                                return (
                                    <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }} key={index}>
                                        <TableCell align="center" component="th" scope="row">
                                            {value.course}
                                        </TableCell>
                                        <TableCell align="center">{value.category}</TableCell>
                                        <TableCell align="center">{moment(value.shedule).locale('id').format('dddd, DD MMMM YYYY [at] HH:mm')}</TableCell>
                                        <TableCell align="center"> IDR {value.price.toLocaleString()}</TableCell>
                                    </TableRow>
                                )
                            })
                        }

                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    )
}