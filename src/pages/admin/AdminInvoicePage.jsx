import { 
    Paper,
    Table, 
    TableBody, 
    TableCell, 
    TableContainer, 
    TableHead, 
    TableRow } from "@mui/material"
// import  FlatButton  from "../components/button/FlatButton";
import { useAdminInvoice } from "../../hooks/useAdminInvoice";
import { useNavigate } from "react-router-dom";
import FlatButton from "../../components/button/FlatButton";
import moment from "moment";
import 'moment/locale/id';

export const AdminInvoicePage = () => {
    const { data, isLoading } = useAdminInvoice ()
    const navigate = useNavigate()
    if (isLoading) {
        return <h1>Loading ...</h1>
    }
    
    return (
        <div>
            <TableContainer component={Paper}>
                <Table sx={{ pb: 15, minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                        <TableCell align="center">Nama</TableCell>
                            <TableCell align="center">No Invoice</TableCell>
                            <TableCell align="center">Tanggal</TableCell>
                            <TableCell align="center">Total Harga</TableCell>
                            <TableCell align="center">Total Produk</TableCell>
                            <TableCell align="center">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            data.map((value, index) => {
                                return (
                                    <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }} key={index}>
                                        <TableCell align="center"> {value.userName}</TableCell>
                                        <TableCell align="center" component="th" scope="row">
                                            {value.noInvoice}
                                        </TableCell>
                                        <TableCell align="center">{moment(value.createdAt).locale('id').format('dddd, DD MMMM YYYY [at] HH:mm')}</TableCell>
                                        <TableCell align="center">IDR {value.totalPrice.toLocaleString()}</TableCell>
                                        <TableCell align="center"> {value.totalOrders}</TableCell>
                                        <TableCell align="center"><FlatButton title='Detail' action={() => navigate(`/admin/admin-detail-invoice/${value.noInvoice}`)} /></TableCell>
                                        
                                    </TableRow>
                                    
                                )
                            })
                        }

                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    )
}