import {
    Box,
    Button,
    FormControl,
    FormControlLabel,
    InputAdornment,
    InputLabel,
    MenuItem,
    Radio,
    RadioGroup,
    Select,
    TextField,
    Typography
} from "@mui/material";
import { useState } from "react";
import { useCreateAdminCourse } from "../../hooks/useCreateAdminCourse";

export const AdminAddCourse = () => {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [formattedPrice, setFormattedPrice] = useState('');
    const [categoryId, setCategoryId] = useState('');
    const [imagePath, setImagePath] = useState('');
    const [isActive, setIsActive] = useState(true);
    const [showImage, setShowImage] = useState(null)

    // const [kategoriOptions, setKategoriOptions] = useState(['test', "hallo1", 'hallo2']);
    const { isLoading, category, actionLoading, createCourse } = useCreateAdminCourse()

    const handleImageChange = (e) => {
        const file = e.target.files[0];

        if (file) {
            const imageUrl = URL.createObjectURL(file);
            setShowImage(imageUrl)
            setImagePath(file);
        }
    };

    const handlePriceChange = (value, setter) => {
        let numericValue = 0; // Default value for null or invalid input
    
        if (value !== null && value !== undefined && value !== '') {
            numericValue = parseFloat(value.replace(/[^0-9]/g, '')); // Remove non-numeric characters
        }
    
        if (numericValue >= 0 && numericValue <= 1000000000) {
            setter(numericValue);
            const formattedValue = numericValue.toLocaleString();
            setFormattedPrice(formattedValue);
        }
    };
    

    // const handlePriceChange = (value, setter) => {
    //     const numericValue = parseFloat(value.replace(/[^0-9]/g, '')); // Remove non-numeric characters
    //     if (numericValue >= 0 && numericValue <= 1000000000) {
    //         setter(numericValue);
    //         const formattedValue = numericValue.toLocaleString();
    //         setFormattedPrice(formattedValue)
    //     }
    // };

    const handleSubmit = (e) => {
        e.preventDefault();

        if (!actionLoading) {
            createCourse(title, description, categoryId, price, imagePath, isActive)
        }
    };

    if (isLoading) {
        return <h1>LOADING ...</h1>
    }

    return (
        <Box p={3}>
            <form autoComplete="off" onSubmit={handleSubmit}>
                <Typography variant='h5' marginBottom={'14px'}>Create Course</Typography>
                <TextField
                    label="Title"
                    required
                    variant="outlined"
                    color="secondary"
                    size="small"
                    fullWidth
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                    sx={{ mb: 2 }}
                />
                <TextField
                    label="Description"
                    required
                    variant="outlined"
                    color="secondary"
                    size="small"
                    fullWidth
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    sx={{ mb: 2 }}
                />
                <TextField
                    label="Price"
                    required
                    variant="outlined"
                    color="secondary"
                    size="small"
                    fullWidth
                    value={formattedPrice}
                    onChange={(e) => handlePriceChange(e.target.value, setPrice)}
                    InputProps={{
                        startAdornment: <InputAdornment position="start">IDR </InputAdornment>,
                    }}
                    sx={{ mb: 2 }}
                />
                <FormControl fullWidth sx={{ mb: 2 }}>
                    <InputLabel id="kategori-label">Kategori</InputLabel>
                    <Select
                        labelId="kategori-label"
                        id="kategori"
                        size="small"
                        value={categoryId}
                        label="Kategori"
                        onChange={(e) => setCategoryId(e.target.value)}
                    >
                        {category.map((kategori) => (
                            <MenuItem key={kategori.id} value={kategori.id}>
                                {kategori.name}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <Box sx={{ mt: 3, display: 'flex', flexDirection: 'row', alignItems: 'center', gap: '50px', }}>
                    <TextField
                        required
                        type="file"
                        inputProps={{ accept: 'image/*' }}
                        onChange={handleImageChange}
                    />
                    <Box>
                        <Box
                            component="img"
                            sx={{ height: '140px', display: showImage == null ? 'none' : 'block' }}
                            src={showImage}
                        />
                    </Box>
                </Box>

                <Box marginTop={'16px'} >
                    <RadioGroup
                        row
                        name="status"
                        value={isActive}
                        onChange={(e) => setIsActive(e.target.value === 'true')}
                    >
                        <FormControlLabel
                            value="true"
                            control={<Radio color="primary" />}
                            label="Aktif"
                        />
                        <FormControlLabel
                            value="false"
                            control={<Radio color="primary" />}
                            label="Tidak Aktif"
                        />
                    </RadioGroup>
                </Box>
                <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    style={{ marginTop: '16px' }}
                >
                    {actionLoading ? "Loading" : "Save"}
                </Button>
            </form>
        </Box>
    )
}