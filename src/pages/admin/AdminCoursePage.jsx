import { Box, Button, InputAdornment, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material"
import { TextField, Select, MenuItem, InputLabel, FormControl } from '@mui/material';
import { Link } from "react-router-dom"
import { useAdminCourse } from "../../hooks/useAdminCourse"
import { useEffect, useState } from "react";

export const AdminCoursePage = () => {
    const { isLoading, setLoading, actionLoading, data, updateStatus } = useAdminCourse();
    const [searchTerm, setSearchTerm] = useState('');
    const [priceHigherThan, setPriceHigherThan] = useState('000000');
    const [priceLowerThan, setPriceLowerThan] = useState('200000000');
    const [formattedPriceHigherThan, setFormattedPriceHigherThan] = useState('');
    const [formattedPriceLowerThan, setFormattedPriceLowerThan] = useState('');
    const [statusFilter, setStatusFilter] = useState('');
    const [debouncedSearchTerm, setDebouncedSearchTerm] = useState('');

    useEffect(() => {
        const timeoutId = setTimeout(() => {
            setDebouncedSearchTerm(searchTerm);
        }, 2500);

        return () => {
            clearTimeout(timeoutId);
        };
    }, [searchTerm,statusFilter,priceHigherThan,priceLowerThan]);

    if (isLoading) {
        return <h1>Loading...</h1>;
    }

    const filteredData = data.filter((value) => {
        // Filter by title
        const titleMatch = value.name.toLowerCase().includes(debouncedSearchTerm.toLowerCase());

        // Filter by price
        const priceMatch =
            (!priceHigherThan || (value.price && value.price > parseFloat(priceHigherThan))) &&
            (!priceLowerThan || (value.price && value.price < parseFloat(priceLowerThan)));

        // Filter by status
        const statusMatch =
            statusFilter === '' || value.isActive.toString() === statusFilter;

        return titleMatch && priceMatch && statusMatch;
    });

    const handlePriceChange = (value, setter) => {
        const numericValue = parseFloat(value.replace(/[^0-9]/g, '')); // Remove non-numeric characters
        if (!isNaN(numericValue) && numericValue >= 0 && numericValue <= 1000000000) {
            setter(numericValue);
            const formattedValue = numericValue.toLocaleString();
            setter === setPriceHigherThan
                ? setFormattedPriceHigherThan(formattedValue)
                : setFormattedPriceLowerThan(formattedValue);
        }
    };

    function truncate(text, maxLength) {
        if (text?.length > maxLength) {
            return text.substring(0, maxLength - 3) + "...";
        } else {
            return text;
        }
    }

    return (
        <Box sx={{ display:'flex', justifyContent:'center', flexDirection:'column', gap:'10px'}}>
            <div>
                <TextField
                    type="text"
                    label="Search by title"
                    size="small"
                    variant="outlined"
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e.target.value)}
                />
                <TextField
                    type="text" // Use type="text" to allow non-numeric characters
                    label="Filter by price higher than"
                    size="small"
                    variant="outlined"
                    value={formattedPriceHigherThan}
                    onChange={(e) => handlePriceChange(e.target.value, setPriceHigherThan)}
                    InputProps={{
                        startAdornment: <InputAdornment position="start">IDR </InputAdornment>,
                    }}
                />
                <TextField
                    type="text" // Use type="text" to allow non-numeric characters
                    label="Filter by price lower than"
                    size="small"
                    variant="outlined"
                    value={formattedPriceLowerThan}
                    onChange={(e) => handlePriceChange(e.target.value, setPriceLowerThan)}
                    InputProps={{
                        startAdornment: <InputAdornment position="start">IDR </InputAdornment>,
                    }}
                />
                <FormControl variant="outlined" size="small" style={{ minWidth: 120 }}>
                    <InputLabel id="status-filter-label">Filter by status</InputLabel>
                    <Select
                        label="Filter by status"
                        labelId="status-filter-label"
                        value={statusFilter}
                        onChange={(e) => setStatusFilter(e.target.value)}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        <MenuItem value="true">Active</MenuItem>
                        <MenuItem value="false">Archived</MenuItem>
                    </Select>
                </FormControl>
            </div>

            <Link to="../add-course">
                <Button
                    variant="contained"
                    color="primary"
                    style={{ marginBottom: '16px' }}>
                    Add Course
                </Button>
            </Link>
            <TableContainer component={Paper}>
                <Table sx={{ pb: 15, minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center" >No</TableCell>
                            <TableCell align="center" >Title</TableCell>
                            <TableCell align="center" >Deskripsi</TableCell>
                            <TableCell align="center" >Harga</TableCell>
                            <TableCell align="center" >Status</TableCell>
                            <TableCell align="center" >Action</TableCell>
                            <TableCell align="center" >Archive</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            filteredData.map((value,index) => {
                                return (
                                    <TableRow
                                        key={value.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                        <TableCell align="center">{index+1}</TableCell>
                                        <TableCell align="center">{value.name}</TableCell>
                                        <TableCell align="center">{truncate(value.description, 100)}</TableCell>
                                        <TableCell align="center">IDR {value.price.toLocaleString()}</TableCell>
                                        <TableCell align="center">{value.isActive ? "Active" : "Archived"}</TableCell>
                                        <TableCell align="center">
                                            <Link to={`../edit-cource/${value.id}`}>
                                                <Button variant="contained">Edit</Button>
                                            </Link>
                                        </TableCell>
                                        <TableCell align="center">

                                            <Button
                                                variant="contained"
                                                color="secondary"
                                                onClick={() => updateStatus(!value.isActive, value.id)}
                                            >
                                                {
                                                    actionLoading ? "Loading" :
                                                        value.isActive ? "Deactivate" : "Activate"
                                                }
                                            </Button>

                                        </TableCell>

                                    </TableRow>

                                )
                            })
                        }
                    </TableBody>
                </Table>
            </TableContainer>
        </Box>
    )
}