import { 
    Button, 
    Paper, 
    Table, 
    TableBody, 
    TableCell, 
    TableContainer, 
    TableHead, 
    TableRow } from "@mui/material"
import { Link } from "react-router-dom"
import { useAdminCategory } from "../../hooks/useAdminCategory"

export const AdminCategoryPage = () => {
    const { actionLoading, data, isLoading, updateStatus} = useAdminCategory()

    if (isLoading) {
        return<h1>Loading . . .</h1> 
    }

    function truncate(text, maxLength) {
        if (text?.length > maxLength) {
          return text.substring(0, maxLength - 3) + "...";
        } else {
          return text;
        }
      }

    return (
        <div>
            <Link to="../add-category">
                <Button
                    variant="contained"
                    color="primary"
                    style={{ marginBottom: '16px' }}
                >
                    Tambah Category
                </Button>
            </Link>
            <TableContainer component={Paper}>
                <Table sx={{ pb: 15, minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center">Name</TableCell>
                            <TableCell align="center">Deskripsi</TableCell>
                            <TableCell align="center">Status</TableCell>
                            <TableCell align="center">Action</TableCell>
                            <TableCell align="center">Status</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            data.map(value => {
                                return (
                                    <TableRow
                                        key={value.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell align="center" component="th" scope="row">
                                            {value.name}
                                        </TableCell>
                                        <TableCell align="center">{truncate(value.description,100)}</TableCell>
                                        <TableCell align="center">
                                            {value.isActive ? "Active" :  "Archived" }
                                        </TableCell>
                                        <TableCell align="center">
                                            <Link to={`../edit-category/${value.id}`}>
                                                <Button variant="contained">Edit</Button>
                                            </Link>
                                        </TableCell>
                                        <TableCell align="center">
                                            <Button
                                                variant="contained"
                                                color="secondary"
                                            onClick={() => updateStatus(!value.isActive, value.id)}
                                            >
                                                {
                                                    actionLoading ? "Loading" : 
                                                    value.isActive ? "Deactivate" : "Activate"
                                                }
                                            </Button>

                                        </TableCell>
                                    </TableRow>


                                )
                            })
                        }
                        
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    )
}