import { 
    Button, 
    Paper, 
    Table, 
    TableBody, 
    TableCell, 
    TableContainer, 
    TableHead, 
    TableRow } from "@mui/material"
import { Link } from "react-router-dom"
import { useAdminSchedule } from "../../hooks/useAdminSchedule"
import moment from "moment"
import 'moment/locale/id';

export const AdminScheulePage = () => {
    const { data, isLoading } = useAdminSchedule()
    

    if (isLoading) {
        return <h1>Loading ...</h1>
    }
    
    return (
        <div>
            <Link to="../add-schedule ">
                <Button
                    variant="contained"
                    color="primary"
                    style={{ marginBottom: '16px' }}
                >
                    Add Schedule Method
                </Button>
            </Link>
            <TableContainer component={Paper}>
                <Table sx={{ pb: 15, minWidth: 650 }} aria-label="schedule table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center">Tanggal</TableCell>
                            <TableCell align="center">Course ID</TableCell>
                            <TableCell align="center">Title</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            data.map(value => {
                                return (
                                    <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }} key={value.id}>
                                        <TableCell align="center" component="th" scope="row">
                                        {moment(value.date).locale('id').format('dddd, DD MMMM YYYY [at] HH:mm')}
                                        </TableCell>
                                        <TableCell align="center">{value.id}</TableCell>
                                        <TableCell align="center">
                                            {value.titleCourse}
                                        </TableCell>
                                    </TableRow>
                                )
                            })
                        }
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    )
}