import { 
    Box, 
    Button, 
    FormControl, 
    FormControlLabel, 
    InputAdornment, 
    InputLabel, 
    MenuItem, 
    Radio, 
    RadioGroup, 
    Select, 
    TextField, 
    Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useCreateAdminCourse } from "../../hooks/useCreateAdminCourse";
import { useEditCourseAdmin } from "../../hooks/useEditCourseAdmin";
import { imgUrl } from "../../utils/api";

export const AdminEditCourse = () => {
    const { id  } = useParams()
    const { isLoading, category, actionLoading, updateCourse} = useCreateAdminCourse()
    const { data, isLoadingProduct} = useEditCourseAdmin(id)

    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [formattedPrice, setFormattedPrice] = useState('');
    const [categoryId, setCategoryId] = useState('');
    const [imagePath, setImagePath] = useState('');
    const [isActive, setIsActive] = useState(true);
    const [showImage, setshowImage] = useState(null);

    const navigate = useNavigate()

    // const [kategoriOptions, setKategoriOptions] = useState(['test', "hallo1", 'hallo2']);
    useEffect(() => {
        if (!isLoadingProduct) {
            setTitle(data.name)
            setDescription(data.description)
            setCategoryId(data.categoryId)
            setPrice(data.price)
            handlePriceChange(data.price.toLocaleString(),setFormattedPrice)
        }
    }, [isLoadingProduct, data])

    const handleImageChange = (e) => {
        const file = e.target.files[0];

        if (file) {
            const imageUrl = URL.createObjectURL(file);
            setshowImage(imageUrl)
            setImagePath (file);
        }
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        if (!actionLoading) {
            updateCourse(title, description, categoryId, price, imagePath, isActive, id)
        }

    };

    const handlePriceChange = (value, setter) => {
        const numericValue = parseFloat(value.replace(/[^0-9]/g, '')); // Remove non-numeric characters
        if (!isNaN(numericValue) && numericValue >= 0 && numericValue <= 1000000000) {
            setter(numericValue);
            const formattedValue = numericValue.toLocaleString();
            setFormattedPrice(formattedValue)
        }
    };

    if (isLoading || isLoadingProduct) {
        return <h1>LOADING ...</h1>
    }


    return (
        <Box p={3}>
            <form autoComplete="off" onSubmit={handleSubmit}>
                <Typography variant='h5' marginBottom={'14px'}>Edit Course</Typography>
                <TextField
                    label="Title"
                    variant="outlined"
                    color="secondary"
                    size="small"
                    fullWidth
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                    sx={{ mb: 2 }}
                />
                <TextField
                    label="Description"
                    variant="outlined"
                    color="secondary"
                    size="small"
                    fullWidth
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    sx={{ mb: 2 }}
                />
                <TextField
                    label="Price"
                    variant="outlined"
                    color="secondary"
                    size="small"
                    fullWidth
                    value={formattedPrice}
                    onChange={(e) => handlePriceChange(e.target.value, setPrice)}
                    InputProps={{
                        startAdornment: <InputAdornment position="start">IDR </InputAdornment>,
                    }}
                    sx={{ mb: 2 }}
                />
                <FormControl fullWidth sx={{ mb: 2 }}>
                    <InputLabel id="kategori-label">Edit Cource</InputLabel>
                    <Select
                        labelId="kategori-label"
                        id="kategori"
                        size="small"
                        value={categoryId}
                        label="Kategori"
                        onChange={(e) => setCategoryId(e.target.value)}
                    >
                        {category.map((kategori) => (
                            <MenuItem key={kategori.id} value={kategori.id}>
                                {kategori.name}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                
                <Box sx={{ mt: 3, display:'flex', flexDirection:'row', alignItems:'center', gap: '50px', }}>
                    <TextField
                        type="file"
                        inputProps={{ accept: 'image/*' }}
                        onChange={handleImageChange}
                    />
                    <Box>
                        <Box
                            component="img"
                            sx={{ height: '140px' }}
                            src={showImage == null ? imgUrl + data.image : showImage}
                        />
                    </Box>
                </Box>

                <Box marginTop={'16px'} >
                    <RadioGroup
                        row
                        name="status"
                        value={isActive}
                        onChange={(e) => setIsActive(e.target.value === 'true')}
                    >
                        <FormControlLabel
                            value="true"
                            control={<Radio color="primary" />}
                            label="Aktif"
                        />
                        <FormControlLabel
                            value="false"
                            control={<Radio color="primary" />}
                            label="Tidak Aktif"
                        />
                    </RadioGroup>
                </Box>
                <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    style={{ marginTop: '16px' }}
                >
                    {actionLoading ? "Loading" : "Save"}
                </Button>
            </form>
        </Box>
    )
}