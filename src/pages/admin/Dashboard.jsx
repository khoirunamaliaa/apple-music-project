import { Box, CardMedia, Typography } from "@mui/material";

export default function Dashboard() {
    return (
        <Box sx={{gap:'10px', display: 'flex', flexDirection:'column', justifyContent: 'center', alignItems: 'left', width:'100%',}}>
            <Typography sx={{ typography: {md:'h1', xs: 'h3' },fontWeight:{md:'600', xs: '500' },}} >
                Welcome 
            </Typography>
            <Typography sx={{ typography: {md:'h3', xs: 'h5' },fontWeight:{md:'500', xs: '400' },}} >
                to Admin Page
            </Typography>
            <CardMedia
            component="img"
            alt="Image" 
            height="100%"
            image="https://res.cloudinary.com/dayhmd2th/image/upload/v1705339717/xwlijktgj07ycpgum2sq.jpg"
            />
        </Box>

    )
}