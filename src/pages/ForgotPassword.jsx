import { Box, Container, Stack, TextField, Typography } from "@mui/material"
import { useState } from "react";
import { gray1Color, gray2Color } from "../utils/color"
import FlatButton from "../components/button/FlatButton"
import { useNavigate } from "react-router-dom"
import BorderButtonForm from "../components/button/BorderButton"
import { useAuth } from "../hooks/useAuth";

export const ForgotPassword = () => {
  const [email, setEmail] = useState("");
  const [error, setError] = useState("");
  const { ForgotPassword, isLoading } = useAuth();

  const navigate = useNavigate()

  const handleCancel = () => {
    navigate("/login")
  }

  const handleConfirmed = async (e) => {
    e.preventDefault()
    const trimmedEmail = email.trim();
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!isLoading) {
      ForgotPassword(email)
    }
    else if (!trimmedEmail) {
      setError("Email is required");
    } else if (!emailRegex.test(trimmedEmail)) {
      setError("Invalid email format");
    }
  }

  return (
    <Container maxWidth="sm">
      <Stack
        // justifyContent={'center'}
        // alignItems={'center'}
        // height={`calc(100vh - 68px)`}
        // width={'100%'}
        // marginTop={'182px'}
        sx={{
          display: "flex",
          flexDirection: "column",
          marginTop: '182px',
          gap: "40px",
          height: "439px",
          overflowY: "auto",
        }}
      >
        <Box sx={{
          display: "flex",
          justifyContent: "start",
          flexDirection: "column",
          gap: "60px",
        }}
        >
          <Box sx={{
            display: "flex",
            justifyContent: "start",
            flexDirection: "column",
            gap: "16px",
          }}>
            <Typography color={gray1Color} variant="h5">
              Reset Password
            </Typography>
            <Typography variant="subtitle1" color={gray2Color} >
              Silahkan masukan terlebih dahulu email anda
            </Typography>
          </Box>
          <form onSubmit={handleConfirmed} >
            <TextField
              fullWidth
              required
              label="Masukkan Email"
              variant="outlined"
              margin="none"
              size="small"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              error={!!error}
              helperText={error}
            />


            <Box sx={{
              display: "flex",
              justifyContent: "flex-start",
              width: '300px',
              gap: '24px',
              marginTop: '40px'
            }}
            >
              <BorderButtonForm action={handleCancel} title={"Batal"} />
              <FlatButton title={ isLoading ? "Loading" : "Submit"} />
            </Box>
          </form>
        </Box>
      </Stack>
    </Container>
  )
}