import { useContext, useEffect, useState } from "react"
import { AuthContext } from "../context/AuthContext"
import axios from "axios"
import { baseUrl } from "../utils/api"

export const useAdminPayment = () => {
    const { token, AuthLogout } = useContext(AuthContext)

    const [isLoading, setLoading] = useState(true)
    const [actionLoading, setActionLoading] = useState(false)
    const [data, setData] = useState(null)

    const getDataAdminPayment = async () => {
        try {
            await axios.get(
                baseUrl + "Payment",
                { headers: { 'Authorization': `Bearer ${token}` } }
            ).then(payload => {
                console.log(payload.data)
                setData(payload.data)
                setLoading(false)
            })
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
                setLoading(false)
            }
        }
    }

    useEffect(() => {
        getDataAdminPayment()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const updateStatus = async (isActive, id) => {
        setActionLoading(true)
        try {
            await axios.put(
                baseUrl + `Payment?id=${id}&status=${isActive}`,
                {},
                { headers: { 'Authorization': `Bearer ${token}` } }
            ).then(payload => {
                console.log(payload.data)
                getDataAdminPayment()
                setActionLoading(false)
            })
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
                setActionLoading(false)
            }
        }
    }

    return {
        isLoading,
        actionLoading,
        data,
        updateStatus
    }
}