import { useContext, useState } from 'react';
import axios from 'axios';
import { AuthContext } from '../context/AuthContext';
import { baseUrl } from '../utils/api';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';
import moment from 'moment';

export const useCheckoutFunction = () => {

    const navigate = useNavigate()

    const { token, Authlogout } = useContext(AuthContext)

    const [isLoading, setLoading] = useState(false);

    const CreateDirectInvoice = async (PaymentId, idSchedule) => {
        try {
            setLoading(true)
            await axios.post(`${baseUrl}Invoice/Detail?ScheduleId=${idSchedule}`,
                {
                    "createAt": moment.utc().format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
                    "paymentId": PaymentId
                },
                { headers: { 'Authorization': `Bearer ${token}` } }).then(() => { navigate('/payment-confirmed'); });
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                });
                Authlogout();
            } else if (error.response && error.response.status === 400) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Gagal menambahkan. Hubungi CS",
                });
            }
        } finally {
            setLoading(false)
        }
    }

    const CreateInvoice = async (PaymentId) => {
        try {
            setLoading(true)
            await axios.post(`${baseUrl}Invoice`,
                {
                    "createAt": moment.utc().format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
                    "paymentId": PaymentId
                },
                { headers: { 'Authorization': `Bearer ${token}` } }).then(() => { navigate('/payment-confirmed'); });
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                });
                Authlogout();
            } else if (error.response && error.response.status === 400) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Gagal menambahkan. Hubungi CS",
                });
            }
        } finally {
            setLoading(false)
        }
    }

    return {
        CreateInvoice,
        CreateDirectInvoice,
        isLoading,
    }
}