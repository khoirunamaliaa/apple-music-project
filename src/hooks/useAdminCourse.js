import { useContext, useEffect, useState } from "react"
import { AuthContext } from "../context/AuthContext"
import axios from "axios"
import { baseUrl } from "../utils/api"

export const useAdminCourse = () => {
    const { token, AuthLogout } = useContext(AuthContext)

    const [isLoading, setLoading] = useState(true)
    const [actionLoading, setActionLoading] = useState(false)
    const [data, setData] = useState(null)

    const getAdminCourseData = async () => {
        try {
            await axios.get(
                baseUrl + "Courses",
                {
                    headers: { 'Authorization': `Bearer ${token}` }
                }
            ).then(payload => {
                setData(payload.data)
                setLoading(false)
            })
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
                setLoading(false)
            }
        }
    }

    useEffect(() => {
        getAdminCourseData()
    }, [])

    const updateStatus = async (isActive, id) => {
        setActionLoading(true)
        try {
            await axios.put(
                baseUrl + `Courses/Active?isActive=${isActive}&id=${id}`,
                {},
                {
                    headers: { 'Authorization': `Bearer ${token}` }
                }
            ).then(payload => {
                getAdminCourseData()
                setActionLoading(false)
            })
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
                setActionLoading(false)
            }
        }
    }

    return {
        isLoading,
        setLoading,
        actionLoading,
        data,
        updateStatus
    }

}