import { useContext, useEffect, useState } from "react"
import { AuthContext } from "../context/AuthContext"
import axios from "axios"
import { baseUrl } from "../utils/api"

export const useAdminDetail = (noInvoice) => {
    const [isLoading, setLoading] = useState(true)
    const [dataAdmin, setDataAdmin] = useState(null)
    const { token, AuthLogout } = useContext(AuthContext)

    const getDataDetailAdmin = async () => {
        try {
            await axios.get(
                baseUrl + `Invoice/AdminDetail?number=${noInvoice}`,
                { headers: { 'Authorization': `Bearer ${token}` } }
            ).then(payload => {
                setDataAdmin(payload.data)
                setLoading(false)
            })
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
                setLoading(false)
            }
        }
    }

    useEffect(() => {
        getDataDetailAdmin()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [noInvoice])
    return {
        isLoading,
        dataAdmin
    }
}