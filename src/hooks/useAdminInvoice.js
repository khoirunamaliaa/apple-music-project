import { useContext, useEffect, useState } from "react"
import { AuthContext } from "../context/AuthContext"
import axios from "axios"
import { baseUrl } from "../utils/api"

export const useAdminInvoice = () => {
    const { token, AuthLogout } = useContext(AuthContext)
    const [isLoading, setLoading] = useState(true)
    const [data, setData] = useState(null)

    const getDataInvoice = async () => {
        try {
            await axios.get(
                baseUrl + `Invoice/Admin`,
                { headers: { 'Authorization': `Bearer ${token}` } }
            ).then(payload => {
                console.log(payload.data)
                setData(payload.data)
                setLoading(false)
            })
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
                setLoading(false)
            }
        }
    }

    useEffect(() => {
        getDataInvoice()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return {
        isLoading,
        data
    }
}