import { useEffect, useState } from 'react';
import axios from 'axios';
import { baseUrl, } from '../utils/api';

export const useListPayment = () =>{
    const [isListLoading, setIsListLoading] = useState(false);
    const [listPayment, setListPayment] = useState([]);

    const getDataClass = async () => {
        try {
            setIsListLoading(true)
            const response = await axios.get(`${baseUrl}Payment?status=true`);
            setListPayment(response.data)
        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
            setIsListLoading(false)
        }
    }

    useEffect(() => {
        getDataClass();
    }, [])
    return{
        isListLoading,
        listPayment,
    }
}
