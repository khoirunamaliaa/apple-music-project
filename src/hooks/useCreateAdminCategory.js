import { useContext, useState } from "react"
import { AuthContext } from "../context/AuthContext"
import axios from "axios"
import { baseUrl } from "../utils/api"
import Swal from "sweetalert2"
import { useNavigate } from "react-router-dom"

export const useCreateAdminCategory = () => {
    const { token, AuthLogout } = useContext(AuthContext)
    const [isLoading, setLoading] = useState(false)
    const navigate = useNavigate();

    const createCategory = async (name, desc, image, status) => {
        setLoading(true)

        const formData = new FormData();
        formData.append('Name', name)
        formData.append('Description', desc);
        formData.append('Image', image);
        formData.append('Status', status);

        try {
            await axios.post(baseUrl + 'Category', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `Bearer ${token}`
                },
            }).then(() => {
                Swal.fire({
                    icon: "success",
                    title: "Success Add Category",
                    showConfirmButton: false,
                    timer: 1500
                });
                navigate('../admin-category');
            });
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again!",
                });
                AuthLogout();
            } else if (error.response && error.response.status === 400) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Gagal menambahkan. Hubungi CS",
                });
            }
        }
        setLoading(false)
    }
    const updateCategory = async (name, desc, image, status, id) => {
        setLoading(true)

        const formData = new FormData();
        formData.append('Name', name);
        formData.append('Description', desc);
        formData.append('Image', image);
        formData.append('Status', status);

        try {
            console.log(formData.get)
            await axios.put(baseUrl + "Category/" + id, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `Bearer ${token}`
                },
            }).then(() => {
                Swal.fire({
                    icon: "success",
                    title: "Success Edit Category",
                    showConfirmButton: false,
                    timer: 1500
                });
                navigate('../admin-category');
            });

        } catch (error) {

            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Something was Wrong",
                });

            }

        }
        setLoading(false)
    }
    return {
        isLoading,
        createCategory,
        updateCategory
    }
}