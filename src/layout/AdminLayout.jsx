
import { Sidebar } from "../drawer/Sidebar";
import { Outlet } from "react-router-dom";
// import Sidebar from "../drawer/Sidebar";

export default function AdminLayout() {
    console.log("ini admin layout")
    return  (
        <Sidebar>
            <Outlet/>
        </Sidebar>
        
    )
  }