import { Checkbox, Box, Typography, IconButton, Container, Grid, Hidden, } from '@mui/material';
import { Delete as DeleteIcon } from '@mui/icons-material';
import { imgUrl } from '../../utils/api';
import { blackColor, irisColor } from '../../utils/color';
import moment from 'moment';
import 'moment/locale/id';
import Swal from 'sweetalert2';

export const CartCard = ({ listCart, handleSelectCart, handleDeleteCart }) => {

  const deleteConfirmation = (cartID) => {
    Swal.fire({
      title: "Are you sure?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then((result) => {
      if (result.isConfirmed) {
        handleDeleteCart(cartID)
        Swal.fire({
          title: "Deleted!",
          text: "Your file has been deleted.",
          icon: "success"
        });
      }
    });
  }

  return (
    <>
      {listCart.length === 0 ? (
        <Typography justifyContent={'center'} textAlign={'center'} variant="body1">
          No Carts available.
        </Typography>
      ) :
        (
          listCart.map((cart) => (
            <Box key={cart.id} display="flex" alignItems="center" mb={1} sx={{ marginBottom: '0px !important', paddingBottom: '15px', borderBottom: '1px solid #E0E0E0' }}>
              <Checkbox checked={cart.isSelected} onChange={() => handleSelectCart(cart.id)} />
              <Grid container spacing={1} onClick={() => handleSelectCart(cart.id)} style={{ cursor: 'pointer' }}>
                <Grid item xs={5} sm={3} key={cart.id}>
                  <Box sx={{
                    width: "100%",
                    aspectRatio: { xs: '1/1', md: "16/10" },
                    backgroundImage: `url(${imgUrl}${cart.image})`,
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    borderRadius: '16px'
                  }} />
                </Grid>
                <Grid item xs={12} sm={9} key={cart.id}>
                  <Box sx={{
                    width: '100%',
                    display: "flex",
                    flexDirection: "column",
                    gap: "8px",
                    paddingLeft:{ xs: '0px', sm: "16px" },
                    paddingRight:{ xs: '5px', sm: "0px" },
                  }}
                  >
                    <Box sx={{
                      width: '100%',
                      display: "flex",
                      flexDirection: "column",
                      gap: "4px",
                    }}>
                      <Typography style={{ variant: 'body1', fontWeight: '400', color: '#828282', textTransform: 'capitalize' }}>
                        {cart.category}
                      </Typography>
                      <Typography style={{ variant: 'h2', fontWeight: '600', color: blackColor() }}>
                        {cart.title}
                      </Typography>
                    </Box>
                    <Typography style={{ variant: 'h6', fontWeight: '600', color: irisColor() }}>
                      {`Jadwal : ${moment(cart.schedule).locale('id').format('dddd, DD MMMM YYYY [at] HH:mm')}`}
                    </Typography>
                    <Typography style={{ variant: 'h6', fontWeight: '600', color: irisColor() }}>
                      {`IDR ${cart.price.toLocaleString()}`}
                    </Typography>
                  </Box>
                </Grid>
                {/* Add the Checkbox component here */}
                {/* <Checkbox checked={cart.isSelected} onChange={(e) => e.stopPropagation()} /> */}
              </Grid>
              <IconButton onClick={() => deleteConfirmation(cart.id)}>
                <DeleteIcon style={{ color: '#EB5757' }} />
                <Hidden smDown>
                  <Typography variant="body1" style={{ marginLeft: '7px', display: 'inline-block', verticalAlign: 'middle' }}>
                    Delete
                  </Typography>
                </Hidden>
              </IconButton>
            </Box>
          ))
        )}
    </>
  )
}