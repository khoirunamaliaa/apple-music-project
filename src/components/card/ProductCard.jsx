import { Box, FormControl, Grid, InputLabel, MenuItem, Select, Stack, Typography } from "@mui/material"
import { blackColor, gray3Color, irisColor } from "../../utils/color"
import { useContext, useState } from "react"
import BorderButtomForm from "../button/BorderButton"
import FlatButton from "../button/FlatButton"
import FlatBorderButton from "../button/FlatBorderButton"
import PaymentDialog from "../modal/PaymentDialog"
import { imgUrl } from "../../utils/api"
import { useCartFunction } from "../../hooks/useCartFunction"
import moment from 'moment';
import 'moment/locale/id';
import { AuthContext } from "../../context/AuthContext"
import Swal from "sweetalert2"

export const ProductCard = ({ Product }) => {
  const { isAuth } = useContext(AuthContext)
  const { addToCart } = useCartFunction();
  const [schedule, setSchedule] = useState('');
  const [openPaymentDialog, setOpenPaymentDialog] = useState(false);

  const handleMasukkanKeranjangClick = () => {
    if (!isAuth) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Silahkan login terlebih dahulu",
      });
    }
    else if (schedule !== null) {
      addToCart(schedule);
    } else {
      // console.error("Please select a schedule before adding to cart.");
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Please select a schedule before adding to cart.",
      });
    }
  };

  const handleBeliSekarangClick = () => {
    if (!isAuth) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Silahkan login terlebih dahulu",
      });
    }
    else if (schedule !== null) {
      setOpenPaymentDialog(true);
    } else {
      console.error("Please select a schedule before adding to cart.");
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Please select a schedule before adding to cart.",
      });
    }
  };

  return (
    <>
      <Box
        sx={{
          width: '100%',
          padding: "100px 5% 0px 5%", gap: '40px', marginTop: '10px'
        }}>
        <Grid container spacing={3} >
          <Grid item xs={12} sm={12} md={5} >
            <Box sx={{
              width: "100%",
              aspectRatio: "16/10",
              backgroundImage: `url(${imgUrl}${Product.image})`,
              backgroundPosition: 'center',
              backgroundSize: 'cover',
              borderRadius: '16px'
            }} />
          </Grid>
          <Grid item xs={12} sm={12} md={7} >
            <Typography variant="body1" sx={{ color: gray3Color(), fontWeight: '400' }}>
              {Product.category}
            </Typography>
            <Typography
              fontWeight={600}
              variant="h5"
              sx={{ color: blackColor() }}
            >
              {Product.name}
            </Typography>
            <Typography
              style={{
                fontWeight: "600",
                color: irisColor(),
                typography: { lg: "h5", md: "h5", sm: "body1", xs: "body1" },
              }}
            >
              IDR {Product.price.toLocaleString()}
            </Typography>

            <Box width={"60%"} marginY={"30px"}>
              <FormControl fullWidth>
                <InputLabel>Date</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={schedule}
                  label="Select Schedule"
                  onChange={(e) => setSchedule(e.target.value)}
                >
                  {Product.schedules.map(scheduleData => (
                    <MenuItem value={scheduleData.id} key={scheduleData.id} >{moment(scheduleData.date).locale('id').format('dddd, DD MMMM YYYY [at] HH:mm')}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
            {/* <Box marginTop={6}>
                      <BorderButtonForm title="Comming Soon" />
                  </Box> */}
            <Stack direction="row" gap={{xs:2, sm:4}} marginTop={5}>
              <FlatBorderButton title="Masukkan Keranjang" action={handleMasukkanKeranjangClick} />
              <FlatButton title="Beli Sekarang" action={handleBeliSekarangClick} />
            </Stack>
            {/* <Box marginTop={6}>
                    <FlatButtonForm title="Login to Buy" action={() => navigate('/login')} />
                  </Box> */}
          </Grid>
        </Grid>
      </Box>
      <PaymentDialog
        open={openPaymentDialog}
        onClose={setOpenPaymentDialog}
        schedule={schedule}
      />
    </>
  )
}