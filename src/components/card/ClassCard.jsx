import { Box, CardContent, Stack, Typography } from "@mui/material"
import { Link } from "react-router-dom"
import { blackColor, gray3Color, irisColor } from "../../utils/color"
import { imgUrl } from "../../utils/api"

export const ClassCard = ({value}) => {
    return (
        <CardContent sx={{
            width: '100%',
            padding: { lg: "10px", md: "10px", sm: "8px 0px", xs: "8px 0px !important" },
        }}>
            <Link to={`/detail-class/${value.id}`} style={{ textDecoration: 'none' }}>
                <Box sx={{
                    width: "100%",
                    aspectRatio: "16/10",
                    backgroundImage: `url(${imgUrl}${value.image})`,
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    border:"0.15rem outset pink",
                    borderRadius: '16px'
                }} />
                <Stack sx={{
                    padding: { lg: "10px", md: "10px", sm: "8px 0px", xs: "8px 0px" },
                    justifyContent: 'space-between',
                    height: { lg: "160px", md: "160px", sm: "130px", xs: "130px" },
                }}>
                    <Box>
                        <Typography variant="body2" color="textPrimary" style={{ fontWeight: '400', color: gray3Color() }}>
                            {value.categoryName}
                        </Typography>
                        <Typography variant="body1" color="textPrimary" style={{ fontWeight: '600', marginBottom: '20px' }}>
                            {value.name}
                        </Typography>

                        <Typography variant="body2" color="textPrimary" style={{ fontWeight: '600', color: '#5D5FEF', marginBottom: '20px' }}>
                            IDR {value.price.toLocaleString()}
                        </Typography>
                    </Box>
                </Stack>
            </Link>
        </CardContent>
    )
}