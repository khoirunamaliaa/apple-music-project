import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, styled, tableCellClasses} from "@mui/material";
import PropTypes from 'prop-types';
import { gray2Color, yellowColor } from "../../utils/color";
import moment from 'moment';
import 'moment/locale/id';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: yellowColor(),
        textColor: gray2Color(),
        color: theme.palette.common.textColor,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme, evenrow }) => ({
    backgroundColor: evenrow === 'true' ? theme.palette.common.white : theme.palette.grey[200],
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

export const DetailInvoiceTable = ({ list, loading }) => {
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 0 }} aria-label="customized table">
                <TableHead>
                    <TableRow>
                        <StyledTableCell >No</StyledTableCell>
                        <StyledTableCell align="left">Couse Name</StyledTableCell>
                        <StyledTableCell align="center">Category</StyledTableCell>
                        <StyledTableCell align="center">Schedule</StyledTableCell>
                        <StyledTableCell align="center">Price</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {loading ? (
                        <StyledTableRow>
                            <StyledTableCell colSpan={6} align="center">
                                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '20vh' }}>
                                    <CircularProgress />
                                </div>
                            </StyledTableCell>
                        </StyledTableRow>
                    ) : (
                        list && list.length > 0 ? (
                            list.map((data, index) => (
                                <StyledTableRow key={index} evenrow={(index % 2 === 0).toString()}>
                                    <StyledTableCell component="th" scope="row">
                                        {index + 1}
                                    </StyledTableCell>
                                    <StyledTableCell align="left">{data.course}</StyledTableCell>
                                    <StyledTableCell align="center">{data.category}</StyledTableCell>
                                    <StyledTableCell align="center">{moment(data.shedule).locale('id').format('dddd, DD MMMM YYYY [at] HH:mm')}</StyledTableCell>
                                    <StyledTableCell align="center">
                                        IDR {data.price ? data.price.toLocaleString() : 'Loading...'}
                                    </StyledTableCell>
                                </StyledTableRow>
                            ))
                        ) : (
                            <StyledTableRow>
                                <StyledTableCell colSpan={5} align="center">
                                    No items available.
                                </StyledTableCell>
                            </StyledTableRow>
                        )
                    )}
                </TableBody>
            </Table>
        </TableContainer >
    )
}