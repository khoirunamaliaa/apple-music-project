import Button from "@mui/material/Button";
import { irisColor } from "../../utils/color";
import PropTypes from "prop-types";

const FlatBorderButton = (Props) => {
  return (
    <Button
      variant="outlined"
      onClick={Props.action}
      style={{
        width : '200px',
        height: '43px',
        border: `1px solid ${irisColor()}`,
        color: irisColor(),
        padding: "10px",
        borderRadius: "8px",
        fontSize: "15px",
        fontWeight: '500',
        lineHeight: '23px',
        textAlign: 'center',
        textTransform: 'none',
      }}
    >
     {Props.title}
    </Button>
  );
};

FlatBorderButton.propTypes = {
  action: PropTypes.func,
  title: PropTypes.string.isRequired,
};

export default FlatBorderButton
