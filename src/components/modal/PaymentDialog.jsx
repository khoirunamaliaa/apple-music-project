import { useState } from 'react';
import PropTypes from 'prop-types';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, Box, CircularProgress, } from '@mui/material';
import { useListPayment } from '../../hooks/useListPayment';
import { imgUrl } from '../../utils/api';
import { useNavigate } from 'react-router-dom';
import { useCheckoutFunction } from '../../hooks/useCheckoutFunction';
import Swal from 'sweetalert2';


const PaymentDialog = (props) => {
  const navigate = useNavigate();
  const { open, onClose, schedule } = props;
  const { CreateInvoice, CreateDirectInvoice } = useCheckoutFunction();

  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState(null);

  const handlePaymentCancel = () => {
    onClose(false);
    setSelectedPaymentMethod(null);
  };

  const handlePaymentConfirm = () => {
    if (!selectedPaymentMethod) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Please select a payment method.",
        customClass: {
          container: 'custom-swal-container-class',
        },
      });
      return;
    }
    if (location.pathname === '/checkout') {
      CreateInvoice(selectedPaymentMethod);
    } else if (location.pathname.startsWith('/detail-class/')) {
      CreateDirectInvoice(selectedPaymentMethod, schedule);
    }
    onClose(false);
  };


  const { listPayment } = useListPayment();

  const handlePaymentMethodClick = (paymentMethod) => {
    setSelectedPaymentMethod((prevMethod) => (prevMethod === paymentMethod ? '' : paymentMethod));;
  };

  if (!listPayment) {
    return (
      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh' }}>
        <CircularProgress />
      </div>
    );
  }

  return (
    <>
    <style>{`
        .custom-swal-container-class {
          z-index: 10000 !important;
        }
    `}</style>
    <Dialog 
    open={open} 
    onClose={handlePaymentCancel}
    >
      <DialogTitle textAlign={'center'} padding={'24px 24px !important'}>Select Payment Method</DialogTitle>
      <DialogContent>
        <Box sx={{
          display: "flex",
          flexDirection: "column",
          gap: '16px'
        }}>
          {listPayment.map((method) => (
            <Button
              key={method.id}
              onClick={() => handlePaymentMethodClick(method.id)}
              variant={selectedPaymentMethod === method.id ? 'contained' : 'Text'}
              color="primary"
              sx={{
                display: 'flex',
                height: '40px',
                fontSize: '18px',
                fontWeight: '500',
                alignItems: 'center',
                justifyContent: 'flex-start',
                textAlign: 'left',
                textTransform: 'none',
                paddingLeft: '56px',
              }}
            >
              <img src={`${imgUrl}${method.image}`} alt={`${method.name} logo`} style={{ marginRight: '8px', position: 'absolute', left: '0', maxHeight: '40px', maxWidth: '40px' }} />
              {method.name}
            </Button>
          ))}
        </Box>
      </DialogContent>
      <DialogActions sx={{ padding: '12px 24px 24px 24px !important' }}>
        <Button onClick={handlePaymentCancel} variant={'outlined'} style={{
          width: '155px',
          height: '48px',
          padding: '12px 16px !important',
          fontSize: '16px',
          fontWeight: '600',
          lineHeight: '24px',
          textAlign: 'center',
          textTransform: 'none',
        }} color="primary">Batal</Button>
        <Button onClick={handlePaymentConfirm} variant={'contained'} style={{
          width: '155px',
          height: '48px',
          padding: '12px 16px !important',
          fontSize: '16px',
          fontWeight: '600',
          lineHeight: '24px',
          textAlign: 'center',
          textTransform: 'none',
        }} color="primary">Bayar</Button>
      </DialogActions>
    </Dialog>
    
    </>
  );
};

PaymentDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default PaymentDialog;
