import { useContext } from "react"
import { AuthContext } from "../../context/AuthContext"
import HeaderAdmin from "./HeaderAdmin"
import { HeaderComp } from "./HeaderComp"
import { useLocation } from "react-router-dom"

export const MainHeader = ({setPathCheck}) => {
    const { isAdmin } = useContext(AuthContext)
    // const location = useLocation();
    
    return (
        <>
        {
            isAdmin  ?  <HeaderAdmin/> : <HeaderComp setPathCheck={setPathCheck}/>
        }
        </>
    )
}