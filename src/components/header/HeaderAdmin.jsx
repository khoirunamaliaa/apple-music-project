import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Container from "@mui/material/Container";
import { Logout } from "@mui/icons-material";
import { blackColor, gray1Color, yellowColor } from "../../utils/color";
import { AuthContext } from "../../context/AuthContext";
import { useNavigate } from "react-router-dom";

function HeaderAdmin() {
  const navigate = useNavigate();
  
  const { AuthLogout } = React.useContext(AuthContext);
  const handleLogout = () => {
    AuthLogout()
    navigate('/login')
  }
  return (
    <AppBar
      position={"fixed"}
      sx={{
        display:'flex',
        justifyContent:'left',
        backgroundColor: yellowColor(),
      }}
    >
      <Box sx={{ color: blackColor() }}>
        <Toolbar
          sx={{
            display: 'flex',
            justifyContent: 'flex-end',
          }}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleLogout}
          >
            <Logout sx={{ color: gray1Color(), margin: "0px 0px 0px 24px", cursor: "pointer" }} />
          </IconButton>

        </Toolbar>
      </Box>
    </AppBar>
  );
}
export default HeaderAdmin;
