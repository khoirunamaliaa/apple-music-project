import { Box, CircularProgress, Grid, Paper, Stack, Typography } from "@mui/material"
import PropTypes from 'prop-types';
import { irisColor, whiteColor } from "../../utils/color"
import { Link } from "react-router-dom"
import { useEffect, useState } from "react"
import axios from "axios"
import { baseUrl, imgUrl } from "../../utils/api";
import { ClassCard } from "../card/ClassCard";

export const ListClass = (Props) => {

    const [loading, setLoading] = useState(true);
    const [classData, setClassData] = useState([]);

    useEffect(() => {
        setLoading(true);
        const fetchData = async () => {
            try {
                const response = await axios.get(`${baseUrl}Courses?limit=20&categoryId=${Props.categoryId}`);
                setClassData(response.data);
            } catch (error) {
                console.error('Error fetching data:', error);
            } finally {
                setLoading(false);
            }
        };

        fetchData();
    }, [Props.categoryId]);

    if (loading) {
        return (
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '782px' }}>
                <CircularProgress />
            </div>
        );
    }

    return (
        <Stack justifyContent={'center'} alignItems={'center'} padding={{ xs: '0px 30px', md: '0px 91px' }} flex={1} style={{
            marginTop: '100px', backgroundColor: whiteColor()
        }}>
            {classData.length === 0 ? (
                <Typography variant="h5" style={{
                    textAlign: 'center',
                    margin: '0 auto',
                    fontWeight: '600',
                    color: irisColor(),
                    marginBottom: '100px'
                }}>
                    Tidak ada kelas {Props.title} yang tersedia
                </Typography>
            ) :
                (<>
                    <Typography variant="h5" style={{
                        textAlign: 'center',
                        margin: '0 auto',
                        fontWeight: '600',
                        color: irisColor(),
                        marginBottom: '20px'
                    }}>
                        Kelas {Props.title} yang tersedia
                    </Typography>
                    <Grid container spacing={4} padding={0} >
                        {classData.map((classItem, index) => (
                            <Grid item xs={12} md={4} key={classItem.id}>
                                <Paper style={{
                                    margin: '10px',
                                    boxShadow: 'none',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    borderRadius: '16px'
                                }}>
                                    <ClassCard value={classItem} />
                                </Paper>
                            </Grid>
                        ))}
                    </Grid>
                </>)}
        </Stack>
    )
}