import { Container, Typography } from "@mui/material";
import { blackColor, gray1Color } from "../../utils/color";
import PropTypes from 'prop-types';

export const TitlewDesc = (Props) => {
    const {
      title,
      desc,
      titleVariant,
      descVariant,
      titleFontWeight,
      descFontWeight,
      textGap,
      topMarginValue,
      textAligning
    } = Props;

    return (
        <Container
          sx={{
            color: 'white',
            marginTop: topMarginValue,
            display:'flex',
            flexDirection: "column",
            paddingLeft : '0px',
            paddingRight : '0px',
            gap : textGap,
          }}
        >
          <Typography
            variant= {titleVariant}
            color="textPrimary"
            sx={{
              textAlign: {xs:'center', md:'left'},
              fontWeight: titleFontWeight,
              color: blackColor(),
              textTransform :'capitalize',
          }}>
            {title}
          </Typography>
          <Typography
            variant= {descVariant}
            style={{
              fontWeight: descFontWeight,
              color: gray1Color(),
              lineHeight : '24px',
              textAlign : textAligning,
          }}>
            {desc}
          </Typography>
        </Container>
      );
}

TitlewDesc.propTypes = {
  title: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  titleVariant: PropTypes.string.isRequired,
  descVariant: PropTypes.string.isRequired,
  titleFontWeight: PropTypes.string,
  descFontWeight: PropTypes.string,
  textGap: PropTypes.string,
  topMarginValue: PropTypes.string,
  textAligning: PropTypes.string,
};