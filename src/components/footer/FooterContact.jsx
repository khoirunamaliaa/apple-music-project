import { Box, Grid, Typography, IconButton } from "@mui/material";
import { blackColor , irisColor, yellowColor } from "../../utils/color";
import { Link } from "react-router-dom";
import { Call, Instagram, YouTube, Telegram, MailOutline, YoutubeSearchedForOutlined } from "@mui/icons-material";

export const FooterContact = () => {
    const recipientEmail = 'cs@gmail.com';
    const phoneNumber = '+62345678901012'; // Replace with your actual phone number
    const subject = '';
    const body = '';

    const composeEmailLink = `mailto:${recipientEmail}?subject=${encodeURIComponent(subject)}&body=${encodeURIComponent(body)}`;
    const callLink = `tel:${phoneNumber}`;

    const iconButtonStyle = {
      borderRadius: '50%', // Make the button circular
      border: '2px solid transparent', // Add a border around the button
      padding: '8px', // Adjust padding as needed
      backgroundColor: irisColor(), // Set background color
    };

    return (
        <Box
          sx={{
            height:'100%px',
            display:'flex',
            flexDirection: "column",
            padding: '0px 24px ',
            gap :'8px',
          }}
        >
          <Typography
            variant="body1"
            sx={{
              textAlign: {xs:'center', md:'left'},
              fontWeight: '500',
              color: blackColor(),
              textTransform :'capitalize',
          }}>
            Kontak
          </Typography>
          <Grid container spacing={4} margin={'0px'} width={'100%'} >
            <Grid item xs={4} md={2.4} sx={{ padding:'8px 0px !important', display: 'flex', justifyContent: 'center' }}>
              <Link to={callLink}>
                <IconButton style={iconButtonStyle}>
                  <Call style={{ backgroundColor: "transparent", color: yellowColor() }} />
                </IconButton>
              </Link>
            </Grid>
            <Grid item xs={4} md={2.4} sx={{ padding:'8px 0px !important', display: 'flex', justifyContent: 'center' }}>
              <Link to="https://www.instagram.com/" target="_blank" rel="noopener noreferrer">
                <IconButton style={iconButtonStyle}>
                  <Instagram style={{ backgroundColor: "transparent", color: yellowColor() }} />
                </IconButton>
              </Link>
            </Grid>
            <Grid item xs={4} md={2.4} sx={{ padding:'8px 0px !important', display: 'flex', justifyContent: 'center' }}>
              <Link to="https://www.youtube.com/" target="_blank" rel="noopener noreferrer">
                <IconButton style={iconButtonStyle}>
                  <YouTube style={{ backgroundColor: "transparent", color: yellowColor()}} />
                </IconButton>
              </Link>
            </Grid>
            <Grid item xs={6} md={2.4} sx={{ padding:'8px 0px !important', display: 'flex', justifyContent: 'center' }}>
              <Link to="https://www.telegram.com/" target="_blank" rel="noopener noreferrer">
                <IconButton style={iconButtonStyle}>
                  <Telegram style={{ backgroundColor: "transparent" , color: yellowColor()}} />
                </IconButton>
              </Link>
            </Grid>
            <Grid item xs={6} md={2.4} sx={{ padding:'8px 0px !important', display: 'flex', justifyContent: 'center' }}>
              <Link to={composeEmailLink}>
                <IconButton style={iconButtonStyle}>
                  <MailOutline style={{ backgroundColor: "transparent", color: yellowColor()}} />
                </IconButton>
              </Link>
            </Grid>
          </Grid>
        </Box>
      );
}