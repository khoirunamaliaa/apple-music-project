import { Box, Container, Typography, List, ListItem, ListItemText } from "@mui/material";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import { blackColor, gray1Color } from "../../utils/color";

export const FooterProductList = ({list}) => {


  const renderProductLink = (category) => (
    <ListItem key={category.id} disablePadding>
      <ListItemText>
        <Link to={`/list-menu/${category.id}`} style={{ textDecoration: 'none' }}>
          <Typography variant="subtitle2" sx={{ fontWeight: '400', color: gray1Color(), lineHeight: '24px', }}>
            {`• ${category.name}`}
          </Typography>
        </Link>
      </ListItemText>
    </ListItem>
  );

  return (
    <Container
      sx={{
        color: 'white',
        display: 'flex',
        flexDirection: "column",
        gap: '0px',
      }}
    >
      <Typography
        variant="body1"
        sx={{
          textAlign: {xs:'center', md:'left'},
          fontWeight: '500',
          color: blackColor(),
          textTransform: 'capitalize',
        }}
      >
        Product
      </Typography>
      <Box sx={{ display: 'flex', flexDirection: 'row', gap: {xs:'0px', lg:'24px'} }}>
        {list.map((column, index) => (
          <List key={index} sx={{ display: 'flex', flexDirection: 'column', gap: '8px', flex: 1, paddingLeft: {xs:'30px', md:'0px'}, }}>
            {column.map((category) => renderProductLink(category))}
          </List>
        ))}
      </Box>
    </Container>
  );
};

FooterProductList.propTypes = {
  list: PropTypes.arrayOf(
    PropTypes.arrayOf(
      PropTypes.shape({
        category: PropTypes.shape({
          name: PropTypes.string.isRequired,
          id: PropTypes.number.isRequired,
        }),
      })
    ),
  ),
};
