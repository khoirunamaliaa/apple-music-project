import { useContext, useEffect } from "react"
import { AuthContext } from "../context/AuthContext"
import { useNavigate } from "react-router-dom"

export const AdminMiddleware = ({children}) => {
    const { isAuth, isAdmin } = useContext(AuthContext)
    const navigate = useNavigate()
    console.log(isAuth)
    console.log(isAdmin)

    useEffect(() => {
        if (!isAdmin) {
            return navigate ('/')
        }
    }, [isAdmin, isAuth, navigate])

    return isAuth && isAdmin ? children : null;
}

