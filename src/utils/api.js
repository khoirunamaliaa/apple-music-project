export const baseUrl = import.meta.env.VITE_API_URL
export const baseLink = import.meta.env.VITE_FE_URL
export const imgUrl = import.meta.env.VITE_API_IMAGE_URL